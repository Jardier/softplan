package br.com.softplan.moduloII.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Exemplo de Mapeamento Muitospara Um
 * 
 * @author JARDIER
 *
 */
@Entity
@Table(name = "empresas", schema = "softplan")
public class Empresa implements Serializable {
	private static final long serialVersionUID = -8328645033971153218L;

	@Id
	@SequenceGenerator(name = "empresas_seq", sequenceName = "softplan.empresas_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(generator = "empresas_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
