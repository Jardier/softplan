package br.com.softplan.moduloII.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "enderecoes", schema = "softplan")
public class Endereco implements Serializable{
	private static final long serialVersionUID = 858568681710124119L;
	
	@Id
	@SequenceGenerator(name="enderecos_seq", sequenceName = "softplan.enderecos_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(generator="enderecos_seq", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@OneToOne(mappedBy = "enderecoEntrega")
	private Pedido pedido;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Endereco other = (Endereco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
