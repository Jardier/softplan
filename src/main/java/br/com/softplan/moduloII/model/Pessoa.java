package br.com.softplan.moduloII.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe que representa a entidade Pessoa
 * @author JARDIER
 *
 */
@Entity
@Table(name = "pessoas", schema = "softplan")
public class Pessoa implements Serializable {
	private static final long serialVersionUID = -4455586982151422419L;

	@Id
	@SequenceGenerator(name = "pessoas_seq", sequenceName = "softplan.pessoas_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(generator = "pessoas_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	@ManyToOne()
	@JoinColumn(name = "empresa_id")
	private Empresa empregador;

	@ManyToMany(mappedBy = "pessoas")
	private Collection<Projeto> projetos = new ArrayList<Projeto>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Empresa getEmpregador() {
		return empregador;
	}

	public void setEmpregador(Empresa empregador) {
		this.empregador = empregador;
	}

	public Collection<Projeto> getProjetos() {
		return projetos;
	}

	public void setProjetos(Collection<Projeto> projetos) {
		this.projetos = projetos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empregador == null) ? 0 : empregador.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (empregador == null) {
			if (other.empregador != null)
				return false;
		} else if (!empregador.equals(other.empregador))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
