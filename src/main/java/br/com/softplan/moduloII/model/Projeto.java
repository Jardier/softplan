package br.com.softplan.moduloII.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Mapeando Muitos para Muitos
 * @author JARDIER
 *
 */
@Entity
@Table(name = "projetos", schema = "softplan")
public class Projeto implements Serializable {
	private static final long serialVersionUID = -966136518063970011L;
	
	@Id
	@SequenceGenerator(name = "projetos_seq", sequenceName = "softplan.projetos_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(generator = "projetos_seq", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@ManyToMany
	@JoinTable(name = "softplan.rel_projetos_pessoas",
			joinColumns = {@JoinColumn(name="projeto_id")},
			inverseJoinColumns = {@JoinColumn(name="pessoa_id")})
	private Collection<Pessoa> pessoas = new ArrayList<Pessoa>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<Pessoa> getPessoas() {
		return pessoas;
	}
	
	public void setPessoas(Collection<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Projeto other = (Projeto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
