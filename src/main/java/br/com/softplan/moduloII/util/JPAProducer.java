package br.com.softplan.moduloII.util;

import javax.annotation.Resource;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import br.com.softplan.moduloIII.qualifiers.MyTransactionManager;

public class JPAProducer {
	
	@PersistenceContext
	@Produces
	private EntityManager em;
	
	@Resource
	@Produces
	@MyTransactionManager
	private UserTransaction tx;
}
