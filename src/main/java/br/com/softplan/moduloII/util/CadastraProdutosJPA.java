package br.com.softplan.moduloII.util;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.softplan.moduloII.model.Categoria;
import br.com.softplan.moduloII.model.Produto;

public class CadastraProdutosJPA {
	public static void main(String[] args) {
		Categoria c1 = new Categoria("Eletrônico");
		Categoria c2 = new Categoria("DVD");
		
	
		
		EntityManager em = PersistenceUtil.geEntityManager();
		EntityTransaction tx = em.getTransaction();
		try{
			tx.begin();
			em.persist(c1);
			em.persist(c2);
			
			Produto p1 = new Produto("001X", "Máquina de Lavar", 1500.00, new Date(), true, c1);
			Produto p2 =  new Produto("002-D","Geladeira", 850.00, new Date(), false, c2);
			
			em.persist(p1);
			em.persist(p2);

			System.out.println("Produtos salvos com sucesso!");
			tx.commit();
			
		} catch(Exception e) {
			tx.rollback();
			System.out.println("Ocorreu um erro ao salvar produto: " + e.getMessage());
		}
		
		PersistenceUtil.close(em);
		PersistenceUtil.close();
	}
}
