package br.com.softplan.moduloII.util;

import javax.persistence.EntityManager;

public class TesteJPA {
	public static void main(String[] args) {

		EntityManager em = PersistenceUtil.geEntityManager();
		System.out.println("Criando banco");
		PersistenceUtil.close(em);
		PersistenceUtil.close();

	}

}
