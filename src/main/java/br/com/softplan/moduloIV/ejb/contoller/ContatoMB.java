package br.com.softplan.moduloIV.ejb.contoller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import br.com.softplan.moduloIV.ejb.GerenciadoEmailLocal;
import br.com.softplan.moduloIV.ejb.model.Email;

@Model
public class ContatoMB implements Serializable{
	private static final long serialVersionUID = -5325812132032158886L;
	
	/*@EJB
	private GerenciadorEmailBean gerenciadorEmail;
	*/
	@EJB(lookup = "java:global/softplan/GerenciadorEmailBean!br.com.softplan.moduloIV.ejb.GerenciadoEmailLocal")
	private GerenciadoEmailLocal gerenciadorEmail;
	
	@Inject
	private Email email;
	
	
	public void enviarEmail() {
		System.out.println("[ContatoMB] Enviando: " + email);
		
		try{
			gerenciadorEmail.enviarMensagem(email);
			
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage(null, new FacesMessage("Email enviado com sucesso!"));
			this.email = null;
			
		} catch(Exception e) {
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao tentar enviar email!", e.getMessage()));
		}
		
	}
	
	public Email getEmail() {
		return email;
	}
	
	public void setEmail(Email email) {
		this.email = email;
	}

}
