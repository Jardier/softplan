package br.com.softplan.moduloIV.ejb;

import java.util.List;

import javax.ejb.Local;

import br.com.softplan.moduloIV.ejb.model.Mensagem;

@Local
public interface GerenciadorMensagensLocal {
	void adicionarMensagem(String usuario, String mensagem);
	List<Mensagem> getMensagens();
}
