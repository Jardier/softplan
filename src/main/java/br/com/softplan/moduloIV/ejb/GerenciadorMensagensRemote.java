package br.com.softplan.moduloIV.ejb;

import javax.ejb.Remote;

@Remote
public interface GerenciadorMensagensRemote {
	void adicionarMensagem(String usuario, String mensagens);
}
