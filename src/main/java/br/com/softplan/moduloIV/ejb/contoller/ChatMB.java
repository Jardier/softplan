package br.com.softplan.moduloIV.ejb.contoller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.InitialContext;

import br.com.softplan.moduloIV.ejb.GerenciadorMensagensRemote;

@SessionScoped
@Named
public class ChatMB implements Serializable {
	private static final long serialVersionUID = 5894100487145317811L;

	private String usuario;
	private String texto;
	private List<String> textos = new LinkedList<String>();

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public List<String> getTextos() {
		List<String> resultado = new ArrayList<>(textos);
		Collections.reverse(resultado);
		return resultado;
	}

	public String acessar() {
		System.out.println("[ChatMB] Acessando chat com usuário: " + usuario);
		texto = null;
		textos.clear();
		return "/moduloIV/chat/chat";
	}

	public void limpar() {
		System.out.println("[ChatMB] Limpando textos");
		textos.clear();
	}

	public void enviar() {
		System.out.println("[ChatMB] Enviando texto: " + texto);
		textos.add(usuario + ": " + texto);

		try {
			InitialContext jndiContext = new InitialContext();
			String jndiName = "java:global/softplan/GerenciadorMensagens!br.com.softplan.moduloIV.ejb.GerenciadorMensagensRemote";

			GerenciadorMensagensRemote gerenciadorMensagens = (GerenciadorMensagensRemote) jndiContext.lookup(jndiName);
			gerenciadorMensagens.adicionarMensagem(usuario, texto);

			texto = null;
		} catch (Exception e) {
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao enviar mensagens usando EJB!",
					e.getMessage()));
		}
	}
}
