package br.com.softplan.moduloIV.ejb;

import javax.ejb.Remote;

import br.com.softplan.moduloIV.ejb.model.Email;

@Remote
public interface GerenciadorEmailRemote {
	public void enviarMensagem(Email email);
}
