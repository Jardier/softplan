package br.com.softplan.moduloIV.ejb;

import javax.ejb.Local;

import br.com.softplan.moduloIV.ejb.model.Email;

@Local
public interface GerenciadoEmailLocal {

	public void enviarMensagem(Email email);
}
