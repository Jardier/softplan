package br.com.softplan.moduloIV.ejb;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.Singleton;

import br.com.softplan.moduloIV.ejb.model.Mensagem;

@Singleton
public class GerenciadorMensagens implements GerenciadorMensagensLocal, GerenciadorMensagensRemote {

	private List<Mensagem> mensagens;
	
	public GerenciadorMensagens() {
		mensagens = new LinkedList<Mensagem>();
	}
	
	public void adicionarMensagem(String usuario, String mensagem) {
		System.out.println("[GerenciadorMensagens] Recebendo mensagem");
		System.out.println("[GerenciadorMensagens] Usuário: " + usuario + ", Mensagem: " + mensagem);
	}
	
	public List<Mensagem> getMensagens() {
		return mensagens;
	}
}
