package br.com.softplan.moduloIV.ejb;

import java.io.Serializable;

import javax.ejb.Stateless;

import br.com.softplan.moduloIV.ejb.model.Email;

@Stateless
public class GerenciadorEmailBean implements GerenciadoEmailLocal, GerenciadorEmailRemote, Serializable {
	private static final long serialVersionUID = 2581833402255988227L;

	public void enviarMensagem(Email email) {
		System.out.println("[GerenciadorEmailBean] Enviando email: " + email);
	}
}
