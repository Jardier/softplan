package br.com.softplan.moduloIV.ejb.schedule;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.ScheduleExpression;
import javax.ejb.Schedules;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;

@Singleton
@Startup
public class AgendamentoBean implements Serializable{
	private static final long serialVersionUID = 1359713678331015986L;
	
	@Resource
	private TimerService timerService;
	
	private Timer timer;
	
	@PostConstruct
	public void init() {
		System.out.println("[AgendamentoBean] Iniciando o EJB com timer");
		System.out.println("[AgendamentoBean] Criando o agendamento em tempo de execução");
		
		ScheduleExpression expression = new ScheduleExpression();
		expression.second(25).minute("*").hour("*");
		
		timer = timerService.createCalendarTimer(expression);
	}
	@Schedules({
		@Schedule(second = "15", minute = "*", hour = "*"),
		@Schedule(second = "0/10", minute = "*", hour = "*")})
	@Timeout
	public void executar() {
		System.out.println("[AgendamentoBean] Executando o agendamento");
		System.out.println("[AgendamentoBean] Executando scheduleexpression " + timer.getTimeRemaining());
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("[AgendamentoBean] Finalizando o agendamento ....");
		timer.cancel();
	}
}
