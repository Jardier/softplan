package br.com.softplan.moduloI.coversores;

import java.io.Serializable;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.softplan.moduloII.model.Categoria;

@FacesConverter(forClass = Categoria.class)
public class CategoriaConversor implements Converter, Serializable {
	private static final long serialVersionUID = -7267595344478087788L;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value != null) {
			return this.getAttributesFrom(component).get(value);
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null && !"".equals(value)) {
			Categoria categoria = (Categoria) value;
			this.addAttribute(component, categoria);

			Long codigo = categoria.getId();
			if (codigo != null) {
				return String.valueOf(codigo);
			}
		}
		return (String) value;
	}

	protected Map<String, Object> getAttributesFrom(UIComponent component) {
		return component.getAttributes();
	}

	protected void addAttribute(UIComponent component, Categoria o) {
		String key = o.getId().toString(); // codigo da empresa como chave neste
											// caso
		this.getAttributesFrom(component).put(key, o);
	}

}
