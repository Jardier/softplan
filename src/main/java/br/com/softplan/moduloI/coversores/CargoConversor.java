package br.com.softplan.moduloI.coversores;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.softplan.moduloI.dao.CargoDAO;
import br.com.softplan.moduloI.model.Cargo;

@FacesConverter("meuConversorCargo")
public class CargoConversor implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Long id = Long.valueOf(value);
		return CargoDAO.getInstance().findById(id);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Cargo cargo = (Cargo) value;
		return String.valueOf(cargo.getId());
	}

}
