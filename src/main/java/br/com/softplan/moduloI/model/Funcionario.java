package br.com.softplan.moduloI.model;

import java.io.Serializable;

public class Funcionario implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String telefone;
	private String dataAdmissao;
	private String salario;
	private String sexo;
	private Cargo cargo = new Cargo();

	public Funcionario() {
		super();
	}

	public Funcionario(String nome, String telefone, String dataAdmissao, String salario, String sexo, Cargo cargo) {
		this();
		this.nome = nome;
		this.telefone = telefone;
		this.dataAdmissao = dataAdmissao;
		this.salario = salario;
		this.sexo = sexo;
		this.cargo = cargo;

	}

	public Funcionario(Long id, String nome, String telefone, String dataAdmissao, String salario, String sexo,
			Cargo cargo) {
		this(nome, telefone, dataAdmissao, salario, sexo, cargo);
		this.id = id;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getSalario() {
		return salario;
	}

	public void setSalario(String salario) {
		this.salario = salario;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Funcionario [id=" + id + ", nome=" + nome + ", telefone=" + telefone + ", dataAdmissao=" + dataAdmissao
				+ ", salario=" + salario + ", sexo=" + sexo + ", cargo=" + cargo + "]";
	}
}
