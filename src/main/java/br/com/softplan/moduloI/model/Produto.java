package br.com.softplan.moduloI.model;

import java.io.Serializable;

public class Produto implements Serializable {
	private static final long serialVersionUID = 3137847167963957977L;

	private Long id;
	private String codigo;
	private String descricao;
	private String preco;
	private Categoria categoria;

	public Produto() {
		super();

	}

	public Produto(String codigo, String descricao, String preco, Categoria categoria) {
		this();
		this.codigo = codigo;
		this.descricao = descricao;
		this.preco = preco;
		this.categoria = categoria;
	}

	public Produto(Long id, String codigo, String descricao, String preco, Categoria categoria) {
		this(codigo, descricao, preco, categoria);
		this.id = id;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getPreco() {
		return preco;
	}

	public void setPreco(String preco) {
		this.preco = preco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Produto [id=" + id + ", codigo=" + codigo + ", descricao=" + descricao + ", preco=" + preco
				+ ", categoria=" + categoria + "]";
	}

}
