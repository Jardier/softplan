package br.com.softplan.moduloI.dao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.softplan.moduloII.model.Categoria;
import br.com.softplan.moduloIII.interceptors.Transacional;
import br.com.softplan.moduloIII.stereotypes.Repository;

@Repository
@Transacional
public class CategoriaDAO {

	@Inject
	private EntityManager em;

	//Usanto InterceptorTransaction
/*	@Inject
	@MyTransactionManager
	UserTransaction tx;*/

	private List<Categoria> categorias;

	@PostConstruct
	public void init() {
		this.categorias = loadAll();
		if (categorias == null || categorias.isEmpty()) {
			save(new Categoria("DVD"));
			save(new Categoria("CD"));
			save(new Categoria("Eletrônico"));
		}
	}

	public List<Categoria> loadAll() {
		List<Categoria> categorias;
		try {
			TypedQuery<Categoria> query = em.createQuery("select c from Categoria c", Categoria.class);
			categorias = query.getResultList();
		} catch (Exception e) {
			throw new RuntimeException("Erro ao carregar todas as catagorias.", e);
		}
		return categorias;
	}

	public Categoria findById(Long id) {
		if (id == null)
			throw new IllegalArgumentException("Um id válido é obrigatório");

		Categoria categoria;

		try {
			categoria = em.find(Categoria.class, id);

		} catch (Exception e) {
			throw new RuntimeException("Ocorreu um erro ao buscar uma categoria por id: " + id, e);
		}
		return categoria;
	}

	public Categoria save(Categoria categoria) {
		if (categoria == null)
			throw new IllegalArgumentException("Não é possível salvar categoria sem dados");

		try {
			if (categoria.getId() != null) {
				em.merge(categoria);
			} else {
				em.persist(categoria);
			}

		} catch (Exception e) {
			throw new RuntimeException("Ocorreu um erro ao salvar uma categoria. ", e);

		}

		return categoria;
	}

	public void remover(Categoria categoria) {
		if (categoria == null)
			throw new IllegalArgumentException("Uma categoria deve ser informada.");

		Long id = categoria.getId();
		if (id == null)
			throw new IllegalArgumentException("Um id válido é obrigatório");

		try {
			Categoria categoriaRemoved = em.find(Categoria.class, id);
			em.remove(categoriaRemoved);
		} catch (Exception e) {
			throw new RuntimeException("Ocorreu um erro ao remover uma categoria de id: " + categoria.getId(), e);
		}
	}

}
