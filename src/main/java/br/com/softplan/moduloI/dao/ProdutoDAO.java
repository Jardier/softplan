package br.com.softplan.moduloI.dao;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import br.com.softplan.moduloII.model.Produto;
import br.com.softplan.moduloIII.qualifiers.MyTransactionManager;
import br.com.softplan.moduloIII.stereotypes.Repository;

@Repository
public class ProdutoDAO {

	@Inject
	private EntityManager em;

	@Inject
	@MyTransactionManager
	private UserTransaction tx;

	@Inject
	private CategoriaDAO categoriaDAO;

	private List<Produto> produtos;

	@PostConstruct
	private void init() {
		this.produtos = loadAll();
		if (produtos == null || produtos.isEmpty()) {
			save(new Produto("001X", "MacBook Pro 17pol", 10.50, new Date(),
					true, categoriaDAO.findById(13L)));
			save(new Produto("002X", "IPad 64MB", 80.50, new Date(), true,
					categoriaDAO.findById(13L)));
			save(new Produto("003X", "IPhone 5", 70.50, new Date(), true,
					categoriaDAO.findById(13L)));
		}

	}

	public List<Produto> loadAll() {
		List<Produto> produtos = null;
		try {
			tx.begin();
			TypedQuery<Produto> query = em.createQuery(
					"select p from Produto p", Produto.class);
			produtos = query.getResultList();
			tx.commit();

		} catch (Exception e) {
			try {
				tx.setRollbackOnly();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}
			throw new RuntimeException(
					"Ocorreu um erro ao carregar todos os produtos.", e);
		}
		return produtos;
	}

	public Produto findById(Long id) {
		if (id == null)
			throw new IllegalArgumentException("Um id válido é obrigatório.");

		Produto produto;
		try {
			tx.begin();
			produto = em.find(Produto.class, id);
			tx.commit();

		} catch (Exception e) {
			try {
				tx.setRollbackOnly();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}
			throw new RuntimeException(
					"Ocorreu um erro ao carregar o produto com o id: " + id, e);
		}

		return produto;
	}

	public Produto save(Produto produto) {
		if (produto == null)
			throw new IllegalArgumentException(
					"Não é prossível salvar um produto sem dados");

		try {
			tx.begin();
			if (produto.getId() == null)
				em.persist(produto);
			em.merge(produto);
			tx.commit();
		} catch (Exception e) {
			try {
				tx.setRollbackOnly();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}
			throw new RuntimeException("Ocorreu um erro ao saval o produto.", e);
		}
		return produto;
	}

	public void remove(Produto produto) {
		if (produto == null)
			throw new IllegalArgumentException("Um produto deve ser informado.");

		Long id = produto.getId();
		if (id == null)
			throw new IllegalArgumentException("Um id válido é obrigatório");

		try {
			tx.begin();
			Produto produtoRemover = em.find(Produto.class, id);
			
			em.remove(produtoRemover);
			tx.commit();
		} catch (Exception e) {
			try {
				tx.setRollbackOnly();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}
			throw new RuntimeException(
					"Ocorreu um erro ao remover um produto com o id: " + id, e);
		}

	}
}
