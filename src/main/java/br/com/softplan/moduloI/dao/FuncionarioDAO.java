package br.com.softplan.moduloI.dao;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.softplan.moduloI.model.Funcionario;
import br.com.softplan.moduloIII.stereotypes.Repository;

@Repository
public class FuncionarioDAO {
	private static FuncionarioDAO instance;
	private Map<Long, Funcionario> storage;
	private int count;

	private FuncionarioDAO() {
		storage = new LinkedHashMap<Long, Funcionario>();
		init();
	}

	private final void init() {
		CargoDAO daoCargo = CargoDAO.getInstance();
		storage.put(1L, new Funcionario(1L, "Kirk", "(11) 1234-5678", "01/01/2013", "100000", "M", daoCargo.findById(1L)));
		storage.put(2L, new Funcionario(2L, "McCoy", "(11) 4321-5678", "01/02/2013", "10000", "M", daoCargo.findById(5L)));
		storage.put(3L, new Funcionario(3L, "Scott", "(11) 4321-8765", "01/03/2013", "10000", "M", daoCargo.findById(5L)));
		storage.put(4L, new Funcionario(4L, "Uhura", "(11) 4321-8765", "01/03/2013", "50000", "F", daoCargo.findById(6L)));
		storage.put(5L, new Funcionario(5L, "Spock", "(11) 1256-3478", "01/01/2013", "80000", "M", daoCargo.findById(1L)));
		storage.put(6L, new Funcionario(6L, "Chekov", "(11) 3478-1256", "01/02/2013", "10000", "M", daoCargo.findById(6L)));
		storage.put(7L, new Funcionario(7L, "Sulu", "(11) 1212-2121", "10/01/2013", "10000", "M", daoCargo.findById(3L)));
		count = storage.size();
	}

	public static FuncionarioDAO getInstance() {
		if (instance == null)
			instance = new FuncionarioDAO();

		return instance;
	}

	public List<Funcionario> loadAll() {
		return new ArrayList<Funcionario>(storage.values());
	}

	public Funcionario findById(Long id) {
		return storage.get(id);
	}

	public Funcionario save(Funcionario funcionario) {
		if (funcionario == null)
			throw new IllegalArgumentException();

		if (funcionario.getId() == null)
			funcionario.setId((long) (++count));

		storage.put(funcionario.getId(), funcionario);
		return funcionario;
	}

	public void remover(Funcionario funcionario) {
		if (funcionario == null)
			throw new IllegalArgumentException();

		if (funcionario.getId() == null)
			throw new IllegalArgumentException();

		storage.remove(funcionario.getId());

	}
}
