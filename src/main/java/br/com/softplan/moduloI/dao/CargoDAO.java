package br.com.softplan.moduloI.dao;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.softplan.moduloI.model.Cargo;
import br.com.softplan.moduloIII.stereotypes.Repository;

//@ManagedBean Usaremos nosso Stereotype
@Repository
public class CargoDAO {
	private static CargoDAO intance;
	private Map<Long, Cargo> storage;
	private int count;

	private CargoDAO() {
		storage = new LinkedHashMap<Long, Cargo>();
		init();
	}

	public final void init() {
		storage.put(1L, new Cargo(1L, "Gerente"));
		storage.put(2L, new Cargo(2L, "Diretor"));
		storage.put(3L, new Cargo(3L, "Secretária"));
		storage.put(4L, new Cargo(4L, "Analista de Sistema"));
		storage.put(5L, new Cargo(5L, "Programador"));
		storage.put(6L, new Cargo(6L, "Arquiteto Software"));
		count = storage.size();
	}

	public static CargoDAO getInstance() {
		if (intance == null)
			intance = new CargoDAO();
		return intance;
	}

	public List<Cargo> loadAll() {
		return new ArrayList<Cargo>(storage.values());
	}

	public Cargo findById(Long id) {
		return storage.get(id);
	}

	public Cargo save(Cargo cargo) {
		if (cargo == null)
			throw new IllegalArgumentException();
		if (cargo.getId() == null)
			cargo.setId((long) (++count));

		storage.put(cargo.getId(), cargo);
		return cargo;
	}
}
