package br.com.softplan.moduloI.helper;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;

public class Auditoria implements ValueChangeListener {

	@Override
	public void processValueChange(ValueChangeEvent event) throws AbortProcessingException {
		System.out.println("Processando evento de mudança de valor em [Auditoria]");
		System.out.println("Valor antes: " + event.getOldValue());
		System.out.println("Valor depois: " + event.getNewValue());
		
	}

}
