package br.com.softplan.moduloI.helper;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.softplan.moduloI.dao.CategoriaDAO;
import br.com.softplan.moduloII.model.Categoria;

public class CategoriaProducer {

	@Inject
	private CategoriaDAO categoriaDAO;

	@Produces
	@Named("categoriasItens")
	@RequestScoped
	public List<SelectItem> getCategorias() {
		System.out.println("Criando lista de itens de categoria [CategoriaProducer]");
		List<Categoria> categorias = categoriaDAO.loadAll();
		List<SelectItem> itens = new ArrayList<SelectItem>();
		for (Categoria c : categorias) {
			itens.add(new SelectItem(c, c.getDescricao()));
		}

		return itens;
	}
	
	
	public void dispposes(@Disposes List<SelectItem> itens) {
		System.out.println("[CategoriaProducer] Destruindo Litsa de categorias");
		System.out.println("Itens: " + itens);
	}
}
