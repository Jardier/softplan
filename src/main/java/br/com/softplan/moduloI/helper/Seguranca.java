package br.com.softplan.moduloI.helper;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

public class Seguranca implements ActionListener {

	@Override
	public void processAction(ActionEvent event) throws AbortProcessingException {
		System.out.println("Processando evento de ação em [Seguranca]");
		System.out.println("Componente selecionado: " + event.getComponent().toString());
		
	}

}
