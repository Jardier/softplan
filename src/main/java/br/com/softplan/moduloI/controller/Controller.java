package br.com.softplan.moduloI.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.softplan.moduloII.model.Categoria;


@ManagedBean
@Named
@SessionScoped
public class Controller implements Serializable {
	private static final long serialVersionUID = -756779740745040007L;

	private String categoria;

	@PersistenceContext
	private EntityManager em;

	public Controller() {
		System.out.println("Instânciando o [Controller]");
	}

	public void metodo() {
		System.out.println("[Controller] Executando o método.");
		System.out.println("[Controller] Entity<Manager: " + em);
		Categoria categoria = em.find(Categoria.class, 34L);
		System.out.println("Categoria: " + categoria.getDescricao());
		this.categoria = categoria.getDescricao();
		TypedQuery<Categoria> query = em.createQuery("select c from Categoria c", Categoria.class);
		List<Categoria> lista = query.getResultList();
		System.out.println(lista.size());
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
}
