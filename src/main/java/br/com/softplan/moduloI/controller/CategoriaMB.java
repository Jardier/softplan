package br.com.softplan.moduloI.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;

import br.com.softplan.moduloI.dao.CategoriaDAO;
import br.com.softplan.moduloII.model.Categoria;
import br.com.softplan.moduloIII.interceptors.Logger;
import br.com.softplan.moduloIII.stereotypes.Controller;

@Controller
@ConversationScoped
public class CategoriaMB implements Serializable {
	private static final long serialVersionUID = 1L;

	private Categoria categoria = new Categoria();
	private DataModel<Categoria> categorias;

	@Inject
	private CategoriaDAO categoriaDAO;

	@Inject
	private Conversation conversation;

	public DataModel<Categoria> getCategorias() {
		if (this.categorias == null) {
			List<Categoria> lista = categoriaDAO.loadAll();
			categorias = new ListDataModel<>(lista);

		}
		return categorias;
	}

	@Logger
	public void salvar() {
		System.out.println("Execuntando o salvar...");
		FacesContext ctx = FacesContext.getCurrentInstance();
		try {
			categoriaDAO.save(this.categoria);
			this.categoria = new Categoria();
			ctx.addMessage(null, new FacesMessage("Categoria salva com sucesso!"));

			if (!conversation.isTransient())
				conversation.end();

			this.categoria = null;

		} catch (Exception e) {
			ctx.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao salvar categoria", e.getMessage()));
		}
	}

	public String novo() {
		if (conversation.isTransient())
			conversation.begin();

		this.categoria = new Categoria();
		return "/moduloI/categoria/cadastrar";
	}

	public String editar() {
		if (!conversation.isTransient())
			conversation.begin();

		this.categoria = categorias.getRowData();
		return "/moduloI/categoria/cadastrar";
	}

	public void excluir() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		try {
			categoriaDAO.remover(categorias.getRowData());
			ctx.addMessage(null, new FacesMessage("Categoria removida com sucesso."));

			if (!conversation.isTransient())
				conversation.end();

			this.categorias = null;
		} catch (Exception e) {
			ctx.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao remover um categoria ", e.getMessage()));

		}

	}

	public void valueAlterado(ValueChangeEvent evento) {
		System.out.println("Processando o evento de mudança de valor em [valueAlterado]");
		System.out.println("Valor antes: " + evento.getOldValue());
		System.out.println("Valor depois: " + evento.getNewValue());
	}

	public void botaoPressionado(ActionEvent evento) {
		System.out.println("Processando o evento de ação em [botaoPressionado]");
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
}
