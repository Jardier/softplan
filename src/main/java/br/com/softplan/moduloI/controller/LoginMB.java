package br.com.softplan.moduloI.controller;

import java.io.Serializable;

import br.com.softplan.moduloIII.stereotypes.Controller;

/*@ManagedBean
@Named*/
@Controller
public class LoginMB implements Serializable{
	private static final long serialVersionUID = -8787444599758209194L;
	
	
	private String login;
	private String senha;
	
	public LoginMB() {
		
	}
	
	public void autenticar() {
		System.out.println("Loging: " + login +"\n"
				+ "Senha: " + senha);
	}
	
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}

}
