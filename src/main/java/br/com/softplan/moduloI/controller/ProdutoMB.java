package br.com.softplan.moduloI.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import br.com.softplan.moduloI.dao.CategoriaDAO;
import br.com.softplan.moduloI.dao.ProdutoDAO;
import br.com.softplan.moduloII.model.Categoria;
import br.com.softplan.moduloII.model.Produto;
import br.com.softplan.moduloIII.stereotypes.Controller;

@Controller
@SessionScoped
public class ProdutoMB implements Serializable {
	private static final long serialVersionUID = 7047490450282580036L;

	@Inject
	private ProdutoDAO produtoDAO;

	@Inject
	private CategoriaDAO categoriaDAO;

	private DataModel<Produto> produtos;
	private Produto prosutoSelecionado;

	public ProdutoMB() {
		super();
		System.out.println("Criando instância de [ProdutoMB]");
		this.prosutoSelecionado = new Produto();
	}

	public DataModel<Produto> getProdutos() {
		if (this.produtos == null) {
			List<Produto> lista = produtoDAO.loadAll();
			produtos = new ListDataModel<>(lista);

		}
		return produtos;
	}

	public Collection<SelectItem> getCategorias() {
		List<Categoria> lista = categoriaDAO.loadAll();
		Collection<SelectItem> itens = new ArrayList<SelectItem>();
		for (Categoria categoria : lista) {
			itens.add(new SelectItem(categoria, categoria.getDescricao()));
		}

		return itens;
	}

	public void salvar() {
		FacesContext ctx = FacesContext.getCurrentInstance();

		try {
			produtoDAO.save(prosutoSelecionado);
			prosutoSelecionado = new Produto();
			ctx.addMessage(null, new FacesMessage("Produto salvo com sucesso"));
		} catch (Exception e) {
			ctx.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocorreu um erro ao salvar o produto", "Erro"));
		}
	}

	public String editar() {
		this.prosutoSelecionado = produtos.getRowData();
		return "/moduloI/produto/cadastrar";
	}

	public String novo() {
		return "/moduloI/produto/cadastrar";
	}

	public void excluir() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		try {
			produtoDAO.remove(produtos.getRowData());
			ctx.addMessage(null, new FacesMessage("Produto excluido com sucesso"));
			this.produtos = null;
		} catch (Exception e) {
			ctx.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocorreu um erro ao excluir um produto", "Erro"));
		}
	}

	public void tradatorEvento(AjaxBehaviorEvent event) {
		System.out.println("Executando o método [tradatorEvento]");
		UIInput input = (UIInput) event.getComponent();
		Categoria categoria = (Categoria) input.getValue();
		prosutoSelecionado.setDescricao(categoria.getDescricao());

		if (categoria.getDescricao().equalsIgnoreCase("DVD")) {
			prosutoSelecionado.setPreco(150.50);
		} else {
			prosutoSelecionado.setPreco(0.00);
		}

	}

	public Produto getProdutoSelecionado() {
		return prosutoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionaodo) {
		this.prosutoSelecionado = produtoSelecionaodo;
	}

	public void setProdutos(DataModel<Produto> produtos) {
		this.produtos = produtos;
	}
}
