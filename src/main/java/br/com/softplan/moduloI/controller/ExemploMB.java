package br.com.softplan.moduloI.controller;

import java.io.Serializable;

import br.com.softplan.moduloIII.stereotypes.Controller;


/*@ManagedBean
@Named*/
@Controller
public class ExemploMB implements Serializable{
	private static final long serialVersionUID = -2710964415287298930L;
	
	private String nome;
	
	public ExemploMB() {
		System.out.println("Criando uma instância de ExemploMB");
	}
	
	public void exeutar() {
		System.out.println("Executando o método ....");
		System.out.println("Nome: " + nome);
	}

	public String getNome() {
		System.out.println("Obtendo o nome: " + nome);
		return nome;
	}
	
	public void setNome(String nome) {
		System.out.println("Alterando o nome: " + nome);
		this.nome = nome;
	}
}
