package br.com.softplan.moduloI.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.faces.model.SelectItem;

import br.com.softplan.moduloIII.stereotypes.Controller;

/*@ManagedBean
@Named*/
@Controller
public class ComponenteMB implements Serializable {
	private static final long serialVersionUID = 2375864423040475581L;

	private String sexoSelecionado;
	private boolean receberEmail;
	private ArrayList<String> generosSelecionados;

	
	public void executar() {
		System.out.println("Sexo: " + sexoSelecionado + "\n" +
				"Receber email: " + (receberEmail ? "SIM" : "NÂO") +
				"\n" + "Generos: " + generosSelecionados);
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection getGeneros() {
		Collection generos = new ArrayList();
		generos.add(new SelectItem("Ficcao", "Ficção"));
		generos.add(new SelectItem("Biografia", "Biografia"));
		generos.add(new SelectItem("NaoFiccao", "Não Ficção"));
		
		return generos;
	}
	public ArrayList<String> getGenerosSelecionados() {
		return generosSelecionados;
	}
	
	public void setGenerosSelecionados(ArrayList<String> generosSelecionados) {
		this.generosSelecionados = generosSelecionados;
	}

	public boolean isReceberEmail() {
		return receberEmail;
	}

	public void setReceberEmail(boolean receberEmail) {
		this.receberEmail = receberEmail;
	}

	public String getSexoSelecionado() {
		return sexoSelecionado;
	}

	public void setSexoSelecionado(String sexoSelecionado) {
		this.sexoSelecionado = sexoSelecionado;
	}

}
