package br.com.softplan.moduloI.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

import br.com.softplan.moduloI.dao.CargoDAO;
import br.com.softplan.moduloI.dao.FuncionarioDAO;
import br.com.softplan.moduloI.model.Cargo;
import br.com.softplan.moduloI.model.Funcionario;
import br.com.softplan.moduloIII.stereotypes.Controller;

/*@ManagedBean
@Named*/
@Controller
@SessionScoped
public class FuncionarioMB implements Serializable {
	private static final long serialVersionUID = 1L;

	private Funcionario funcionario = new Funcionario();
	private DataModel<Funcionario> funcionarios;

	public DataModel<Funcionario> getFuncionarios() {
		List<Funcionario> lista = FuncionarioDAO.getInstance().loadAll();
		funcionarios = new ListDataModel<Funcionario>(lista);
		return funcionarios;
	}

	public Collection<SelectItem> getCargosItens() {
		List<Cargo> cargos = CargoDAO.getInstance().loadAll();
		Collection<SelectItem> itens = new ArrayList<SelectItem>();
		for (Cargo c : cargos) {
			itens.add(new SelectItem(c, c.getDescricao()));
		}
		return itens;
	}

	public void salvar() {
		System.out.println("Salvando funcionário ...");
		FacesContext ctx = FacesContext.getCurrentInstance();
		try {
			FuncionarioDAO.getInstance().save(funcionario);
			ctx.addMessage(null, new FacesMessage("Funcinário salvo com sucesso!"));
			this.funcionario = new Funcionario();

		} catch (Exception e) {
			ctx.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao salvar funcionário", e.getMessage()));
		}
	}

	public String novo() {
		funcionario = new Funcionario();
		return "cadastrar";
	}

	public String editar() {
		this.funcionario = funcionarios.getRowData();
		return "editar";
	}

	public void remover() {
		System.out.println("Removendo funcionário");
		FacesContext ctx = FacesContext.getCurrentInstance();
		try {
			Funcionario f = funcionarios.getRowData();
			FuncionarioDAO.getInstance().remover(f);
			ctx.addMessage(null, new FacesMessage("Funcionário removido com sucesso!"));
		} catch (Exception e) {
			ctx.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao remover funcionário", e.getMessage()));
		}
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
}
