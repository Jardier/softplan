package br.com.softplan.moduloIII.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.IllegalLoopbackException;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import br.com.softplan.moduloIII.enums.Banco;
import br.com.softplan.moduloIII.model.ContaBancaria;
import br.com.softplan.moduloIII.qualifiers.MyTransactionManager;
import br.com.softplan.moduloIII.stereotypes.Repository;

@Repository
public class ContaBancariaDAO implements Serializable {
	private static final long serialVersionUID = -7267841695715770197L;

	@Inject
	private EntityManager em;

	@Resource
	@MyTransactionManager
	private UserTransaction tx;

	@PostConstruct
	public void init() {
		List<ContaBancaria> contas = loadAll();
		if (contas == null || contas.isEmpty()) {
			saveOrUpdate(new ContaBancaria(Banco.SOFTPLAN, "001X", 10000.00));
			saveOrUpdate(new ContaBancaria(Banco.SOFTPLAN, "002Y", 1500.00));
			saveOrUpdate(new ContaBancaria(Banco.SPOKNET, "003A", 100.00));
			saveOrUpdate(new ContaBancaria(Banco.SPOKNET, "006H", 500.00));
		}

	}

	public List<ContaBancaria> loadAll() {
		List<ContaBancaria> contas = new ArrayList<ContaBancaria>();
		try {
			tx.begin();
			TypedQuery<ContaBancaria> query = em.createQuery("select cb from ContaBancaria cb", ContaBancaria.class);
			contas = query.getResultList();
			tx.commit();

		} catch (Exception e) {
			try {
				tx.setRollbackOnly();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}

			throw new RuntimeException("Erro ao carregar todas as contas.");
		}
		return contas;
	}

	public ContaBancaria saveOrUpdate(ContaBancaria conta) {
		if (conta == null)
			throw new IllegalArgumentException("Uma conta bancária válida é obrigatória.");
		
		Long id = conta.getId();
		try{
			tx.begin();
			
			if(id == null) {
				em.persist(conta);
			} else {
				conta = em.merge(conta);
			}
			tx.commit();
			
		} catch(Exception e) {
			try {
				tx.setRollbackOnly();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}
			throw new RuntimeException("Erro ao salvar uma conta bancária", e);
		}

		return conta;
	}
	
	public ContaBancaria findByNumeroAndBanco(String numero, Banco banco) {
		if(numero == null || numero.isEmpty())
			throw new IllegalLoopbackException("Um número válido de conta é obrigatótio");
		
		if(banco == null)
			throw new IllegalLoopbackException("Um banco válido de conta é obrigatótio");
		
		ContaBancaria conta = null;
		try{
			tx.begin();
			TypedQuery<ContaBancaria> query = em.createQuery("select cb from ContaBancaria cb "
					+ "where cb.numero = :numero and cb.banco = :banco", ContaBancaria.class);
			query.setParameter("numero", numero);
			query.setParameter("banco", banco);
			
			conta = query.getSingleResult();
			tx.commit();
		} catch(Exception e) {
			try {
				tx.rollback();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}
			throw new RuntimeException("Erro ao carregar a conta. [" + numero + "]" , e);
		}
		return conta;
	}
}
