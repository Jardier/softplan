package br.com.softplan.moduloIII.interceptors.impl;

import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import br.com.softplan.moduloIII.interceptors.Logger;

@Logger
@Interceptor
public class LogInterceptor implements Serializable{
	private static final long serialVersionUID = -1618918448259688151L;
	
	@AroundInvoke
	public Object logar(InvocationContext ctx) throws Exception{
		String nomeMetodo = ctx.getMethod().getName();
		Object componente = ctx.getTarget();
		
		System.out.println("[LogInterceptor] Executando algo antes do método [" + nomeMetodo + "] no componente ["
				+ componente +"]");
		
		Object resultado = ctx.proceed();
		
		System.out.println("[LogInterceptor] Executando algo após do método [" + nomeMetodo + "] no componente ["
				+ componente +"]");
		
		
		return resultado;
		
	}

}
