package br.com.softplan.moduloIII.interceptors.impl;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.UserTransaction;

import br.com.softplan.moduloIII.interceptors.Transacional;
import br.com.softplan.moduloIII.qualifiers.MyTransactionManager;

@Transacional
@Interceptor
public class TransactionInterceptor {

	@Inject
	@MyTransactionManager
	private UserTransaction tx;
	
	@AroundInvoke
	public Object executarComTransacao(InvocationContext ic) throws Exception {
		String nomeMetodo = ic.getMethod().getName();
		
		System.out.println("[TransactionInterceptor] Iniciando TX para o método: " + nomeMetodo);
		
		tx.begin();
		Object resultado;
		
		try{
			resultado = ic.proceed();
			tx.commit();
			
			System.out.println("[TransactionInterceptor] Finalizando TX para o método: " + nomeMetodo);
			
		} catch(Exception e) {
			System.out.println("[TransactionInterceptor] (RollBack) TX rollback para o método: " + nomeMetodo);
			tx.setRollbackOnly();
			throw e;
		}
		
		
		return resultado;
	}

}
