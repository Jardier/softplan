package br.com.softplan.moduloIII.decorator;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import br.com.softplan.moduloIII.facede.ICaixaEletronico;
import br.com.softplan.moduloIII.model.ContaBancaria;
import br.com.softplan.moduloIII.qualifiers.BancoSoftplan;

@Decorator
public abstract class CaixaEletronicoDecorator implements ICaixaEletronico {
	
	@Inject
	@Delegate
	@BancoSoftplan
	private ICaixaEletronico cxEletronico;

	@Override
	public void transferir(String contaOrigem, String contaDestino, double valor) {
		System.out.println("[CaixaEletronicoDecorator] Verificando regras usando decorator.");
		if(valor > 5000.00) 
			throw new IllegalArgumentException("Não é possivel fazer transferências maior que R$ 5.000,00 no banco Softplan");
		
		cxEletronico.transferir(contaOrigem, contaDestino, valor);

	}

	@Override
	public void abrirConta(ContaBancaria conta) {
		System.out.println("[CaixaEletronicoDecorator] Verificando regras usando decorator.");
		if(conta.getSaldo() < 500.00)
			throw new IllegalArgumentException("Não é possivel abrir conta com o valor menor que R$ 500,00 no banco Softplan.");
		cxEletronico.abrirConta(conta);

	}

}
