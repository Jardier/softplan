package br.com.softplan.moduloIII.exception;

public class SaldoInsuficienteException extends RuntimeException {
	private static final long serialVersionUID = 8896035557836806296L;

	public SaldoInsuficienteException() {
		super();
	}

	public SaldoInsuficienteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStracktrace) {
		super(message, cause, enableSuppression, writableStracktrace);
	}

	public SaldoInsuficienteException(String message, Throwable cause) {
		super(message, cause);
	}

	public SaldoInsuficienteException(String message) {
		super(message);
	}

	public SaldoInsuficienteException(Throwable cause) {
		super(cause);
	}
}
