package br.com.softplan.moduloIII.controller;

import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.naming.InitialContext;

import br.com.softplan.moduloIII.enums.Banco;
import br.com.softplan.moduloIII.facede.ICaixaEletronico;
import br.com.softplan.moduloIII.interceptors.Logger;
import br.com.softplan.moduloIII.model.ContaBancaria;
import br.com.softplan.moduloIII.model.TransferenciaBancaria;
import br.com.softplan.moduloIII.qualifiers.BancoSoftplan;
import br.com.softplan.moduloIII.qualifiers.BancoSpocknet;
import br.com.softplan.moduloIII.stereotypes.Controller;
import br.com.softplan.moduloIV.ejb.GerenciadorEmailRemote;
import br.com.softplan.moduloIV.ejb.model.Email;

/*@Named
@RequestScoped*/
//Usaremos no Stereotype
@Controller
public class CaixaMB implements Serializable {
	private static final long serialVersionUID = 6909330016172148303L;

	// Utilizando o EJB Remote, interface. é recomendado nesse caso, a interface
	// Local, pois a aplicação, estão no mesmo container
	
	/*@EJB usaremos agora a injeção via contexto do CDI
	private GerenciadorEmailRemote gerenciadorEmail;*/
	
	@Inject
	private Email email;

	@Inject
	private TransferenciaBancaria transferenciaBancaria;

	@Inject
	@BancoSoftplan
	private ICaixaEletronico softplanCaixaEletronico;

	@Inject
	@BancoSpocknet
	private ICaixaEletronico spoknetCaixaEletronico;

	@Inject // gerenciador de Eventos
	@BancoSoftplan
	private Event<TransferenciaBancaria> gerenciadorEventos;

	private ContaBancaria conta = new ContaBancaria();

	public CaixaMB() {
		System.out.println("Criando instância de [CaixaMB]");

	}

	@Logger
	public void transferir() {
		System.out.println("[CaixaMB] executando transferência bancária.");
		
		try {
		//Injetando via contexto do JNDI
			InitialContext jndiContext = new InitialContext();
			String jndiName = "java:global/softplan/GerenciadorEmailBean!br.com.softplan.moduloIV.ejb.GerenciadorEmailRemote";
			
		GerenciadorEmailRemote	gerenciadorEmail = (GerenciadorEmailRemote) jndiContext.lookup(jndiName);
		//Enviando mensagem de email, usando EJB
		email.setDestinatario("jardier@hotmail.com");
		email.setAssunto("Transferência");
		email.setMensagem("Transferência da conta: " + transferenciaBancaria.getContaOrigem() +
				" no valor de R$ " + transferenciaBancaria.getValor() + " para a conta " + transferenciaBancaria.getContaDestino());
		
		gerenciadorEmail.enviarMensagem(email);
		

			if ("SOFTPLAN".equalsIgnoreCase(transferenciaBancaria.getBanco())) {

				softplanCaixaEletronico.transferir(transferenciaBancaria.getContaOrigem(),
						transferenciaBancaria.getContaDestino(), transferenciaBancaria.getValor());

				System.out.println("[CaixaMB] : Enviando evento de transferência " + transferenciaBancaria);
				gerenciadorEventos.fire(transferenciaBancaria);

			} else if ("SPOKNET".equalsIgnoreCase(transferenciaBancaria.getBanco())) {
				spoknetCaixaEletronico.transferir(transferenciaBancaria.getContaOrigem(),
						transferenciaBancaria.getContaDestino(), transferenciaBancaria.getValor());
			} else {
				throw new UnsupportedOperationException("Banco inválido.");
			}
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage(null, new FacesMessage("Transferência realizada com sucesso."));

			transferenciaBancaria = new TransferenciaBancaria();

		} catch (Exception e) {
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao tentar realizar uma transferência bancária. ", e.getMessage()));
		}
	}

	public void abrirConta() {
		System.out.println("[CaixaMB] Iniciando abertura de conta");
		try {
			if (Banco.SOFTPLAN.equals(conta.getBanco())) {
				System.out.println("Abrir conta no banco [SOFTPLAN]");
				softplanCaixaEletronico.abrirConta(conta);

			} else if (Banco.SPOKNET.equals(conta.getBanco())) {
				System.out.println("Abrir conta no banco [SPOKNET]");
				spoknetCaixaEletronico.abrirConta(conta);

			} else {
				throw new UnsupportedOperationException("Banco inválido.");
			}

			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Conta aberta com sucesso.",
					"[" + conta.getNumero() + "]"));

			conta = new ContaBancaria();

		} catch (Exception e) {
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao tentar abrir a conta: [" + conta + "]", e.getMessage()));
		}
	}

	public ContaBancaria getConta() {
		return conta;
	}

	public void setConta(ContaBancaria conta) {
		this.conta = conta;
	}

	public Banco[] getBancos() {
		return Banco.values();
	}

	public TransferenciaBancaria getTransferenciaBancaria() {
		return transferenciaBancaria;

	}

	public void setTransferenciaBancaria(TransferenciaBancaria transferenciaBancaria) {
		this.transferenciaBancaria = transferenciaBancaria;
	}

}
