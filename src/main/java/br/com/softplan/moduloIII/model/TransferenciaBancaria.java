package br.com.softplan.moduloIII.model;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.enterprise.context.Dependent;

/**
 * Classe que será utilizada como elemento de evento,
 * para quando ocorrer um transferência bancária.
 * @author JARDIER
 *
 */
@ManagedBean
@Dependent
public class TransferenciaBancaria implements Serializable {
	private static final long serialVersionUID = -7906722376167116567L;

	private String banco;
	private String contaOrigem;
	private String contaDestino;
	private double valor;

	public TransferenciaBancaria() {
		super();
	}
	
	public TransferenciaBancaria(String banco, String contaOrigem, String contaDestino, double valor) {
		this();
		this.banco = banco;
		this.contaOrigem = contaDestino;
		this.contaDestino = contaDestino;
		this.valor = valor;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getContaOrigem() {
		return contaOrigem;
	}

	public void setContaOrigem(String contaOrigem) {
		this.contaOrigem = contaOrigem;
	}

	public String getContaDestino() {
		return contaDestino;
	}

	public void setContaDestino(String contaDestino) {
		this.contaDestino = contaDestino;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "TransferenciaBancaria [banco=" + banco + ", contaOrigem=" + contaOrigem + ", contaDestino="
				+ contaDestino + ", valor=" + valor + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((banco == null) ? 0 : banco.hashCode());
		result = prime * result + ((contaOrigem == null) ? 0 : contaOrigem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransferenciaBancaria other = (TransferenciaBancaria) obj;
		if (banco == null) {
			if (other.banco != null)
				return false;
		} else if (!banco.equals(other.banco))
			return false;
		if (contaOrigem == null) {
			if (other.contaOrigem != null)
				return false;
		} else if (!contaOrigem.equals(other.contaOrigem))
			return false;
		return true;
	}

}
