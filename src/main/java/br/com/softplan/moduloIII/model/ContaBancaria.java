package br.com.softplan.moduloIII.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.softplan.moduloIII.enums.Banco;
import br.com.softplan.moduloIII.exception.SaldoInsuficienteException;

@Entity
@Table(name = "contas", schema = "softplan")
public class ContaBancaria implements Serializable {
	private static final long serialVersionUID = -370706484571498619L;

	@Id
	@SequenceGenerator(name = "contas_seq", sequenceName = "softplan.contas_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(generator = "contas_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	private String numero;
	private double saldo;

	@Enumerated(EnumType.STRING)
	private Banco banco;

	public ContaBancaria() {
		super();
	}

	public ContaBancaria(Banco banco, String numero) {
		this();
		this.banco = banco;
		this.numero = numero;
	}

	public ContaBancaria(Banco banco, String numero, double saldoIncial) {
		this(banco, numero);
		this.saldo = saldoIncial;
	}

	// Metodos de negocio
	public void saque(double valor) {
		System.out.println("[ContaBancaria] saque de " + valor + " em conta " + this.numero + " e banco " + this.banco);
		if (valor <= 0)
			throw new IllegalArgumentException("Valor de saque inválido: " + valor);
		
		if(valor > this.saldo)
			throw new SaldoInsuficienteException("Saldo insuficiente. Conta " + this.numero + 
					" com saldo disponível de R$ " + this.saldo);
		
		this.saldo -= valor;
	}
	
	public void depositar(double valor) {
		System.out.println("[ContaBancaria] depósito de " + valor + " em conta " + this.numero + 
				" no banco " + this.banco);
		if (valor <= 0)
		throw new IllegalArgumentException("Valor de saque inválido: " + valor);
		
		this.saldo += valor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaBancaria other = (ContaBancaria) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ContaBancaria [id=" + id + ", numero=" + numero + ", saldo=" + saldo + ", banco=" + banco + "]";
	}

}
