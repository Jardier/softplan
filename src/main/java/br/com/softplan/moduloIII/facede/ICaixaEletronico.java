package br.com.softplan.moduloIII.facede;

import br.com.softplan.moduloIII.model.ContaBancaria;

public interface ICaixaEletronico {
	public void transferir(String contaOrigem, String contaDestino, double valor);
	public void abrirConta(ContaBancaria conta);
}
