package br.com.softplan.moduloIII.facede;

import java.util.List;

import br.com.softplan.moduloIII.model.TransferenciaBancaria;

public interface IExtratoOnline {
	List<TransferenciaBancaria> getTransferencias();
}
