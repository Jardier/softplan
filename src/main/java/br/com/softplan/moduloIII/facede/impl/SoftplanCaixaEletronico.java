package br.com.softplan.moduloIII.facede.impl;

import javax.annotation.ManagedBean;
import javax.inject.Inject;

import br.com.softplan.moduloIII.dao.ContaBancariaDAO;
import br.com.softplan.moduloIII.enums.Banco;
import br.com.softplan.moduloIII.facede.ICaixaEletronico;
import br.com.softplan.moduloIII.interceptors.Logger;
import br.com.softplan.moduloIII.model.ContaBancaria;
import br.com.softplan.moduloIII.qualifiers.BancoSoftplan;

@ManagedBean
@BancoSoftplan
@Logger
public class SoftplanCaixaEletronico implements ICaixaEletronico{
	

	@Inject
	private ContaBancariaDAO contaBancariaDAO;
	
	public SoftplanCaixaEletronico() {
		System.out.println("Criando instância de [SoftplanCaixaEletronico]");
	}

	@Override
	public void transferir(String contaOrigem, String contaDestino, double valor) {
		System.out.println("[SoftplanCaixaEletronico] Transferindo dinheiro com o caixa da Softplan");
		
		ContaBancaria cbOrigem = contaBancariaDAO.findByNumeroAndBanco(contaOrigem, Banco.SOFTPLAN);
		if(cbOrigem == null)
			throw new IllegalArgumentException("Não existe a conta bancária com o número: " + contaOrigem +
					" no Banco: " + Banco.SOFTPLAN);
		
		ContaBancaria cbDestino = contaBancariaDAO.findByNumeroAndBanco(contaDestino, Banco.SOFTPLAN);
		if(cbDestino == null)
			throw new IllegalArgumentException("Não existe a conta bancária com o número: " + cbDestino +
					" no Banco: " + Banco.SOFTPLAN);

		cbOrigem.saque(valor);
		contaBancariaDAO.saveOrUpdate(cbOrigem);
		
		cbDestino.depositar(valor);
		contaBancariaDAO.saveOrUpdate(cbDestino);
		
		System.out.println("[SoftplanCaixaEletronico] transferência realizada com sucesso.");
	}

	@Override
	public void abrirConta(ContaBancaria conta) {
		System.out.println("[SoftplanCaixaEletronico] Abrindo conta com o caixa da Softplan");
		System.out.println("Poderiamos executar alguma regra de negocio");
		contaBancariaDAO.saveOrUpdate(conta);
		
	}
	
	
}
 