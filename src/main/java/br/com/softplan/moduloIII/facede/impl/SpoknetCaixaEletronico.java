package br.com.softplan.moduloIII.facede.impl;

import javax.annotation.ManagedBean;
import javax.inject.Inject;

import br.com.softplan.moduloIII.dao.ContaBancariaDAO;
import br.com.softplan.moduloIII.enums.Banco;
import br.com.softplan.moduloIII.facede.ICaixaEletronico;
import br.com.softplan.moduloIII.model.ContaBancaria;
import br.com.softplan.moduloIII.qualifiers.BancoSpocknet;

@ManagedBean
@BancoSpocknet
public class SpoknetCaixaEletronico implements ICaixaEletronico {

	@Inject
	private ContaBancariaDAO contaBancariaDAO;

	public SpoknetCaixaEletronico() {
		System.out.println("Criando instância de [SpoknetCaixaEletronico]");
	}

	@Override
	public void transferir(String contaOrigem, String contaDestino, double valor) {
		System.out.println("[SpoknetCaixaEletronico] Transferindo dinheiro com o caixa da SpokNet");
		
		ContaBancaria cbOrigem = contaBancariaDAO.findByNumeroAndBanco(contaOrigem, Banco.SPOKNET);
		if(cbOrigem == null)
			throw new IllegalArgumentException("Não existe a conta bancária com o número: " + contaOrigem +
					" no Banco: " + Banco.SPOKNET);
		
		ContaBancaria cbDestino = contaBancariaDAO.findByNumeroAndBanco(contaDestino, Banco.SPOKNET);
		if(cbDestino == null)
			throw new IllegalArgumentException("Não existe a conta bancária com o número: " + cbDestino +
					" no Banco: " + Banco.SPOKNET);
		
		cbOrigem.saque(valor);
		contaBancariaDAO.saveOrUpdate(cbOrigem);
		
	
		cbDestino.depositar(valor);
		contaBancariaDAO.saveOrUpdate(cbDestino);
		
		System.out.println("[SpoknetCaixaEletronico] transferência realizada com sucesso.");
				
	}

	@Override
	public void abrirConta(ContaBancaria conta) {
		System.out.println("[SpoknetCaixaEletronico] Abrindo conta com o caixa da SpokNet");
		System.out.println("Poderiamos executar alguma regra de negocio");
		contaBancariaDAO.saveOrUpdate(conta);
		
	}

}
