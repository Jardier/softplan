package br.com.softplan.moduloIII.facede.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import br.com.softplan.moduloIII.facede.IExtratoOnline;
import br.com.softplan.moduloIII.model.TransferenciaBancaria;
import br.com.softplan.moduloIII.qualifiers.BancoSoftplan;

@ApplicationScoped
public class SoftplanExtratoOnline implements IExtratoOnline {

	@Produces
	@Named("transferenciasSoftplan")
	@RequestScoped
	private List<TransferenciaBancaria> transferencias;

	@PostConstruct
	public void init() {
		transferencias = new ArrayList<TransferenciaBancaria>();
	}

	@Override
	public List<TransferenciaBancaria> getTransferencias() {
		System.out.println("[SoftplanExtratoOnline] Obtendo transferências.");
		return transferencias;
	}
	
	//Registrando o evento de transferencia
	public void registraTransferencia(@Observes @BancoSoftplan TransferenciaBancaria transferencia) {
		if(transferencia != null){
			System.out.println("[SoftplanExtratoOnline] registrando tranerência bancária.");
			transferencias.add(transferencia);
		}
	}

}
