package br.com.softplan.alternatives;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Specializes;

import br.com.softplan.moduloIII.facede.impl.SoftplanCaixaEletronico;

@Alternative
@Specializes
public class TesteCaixaEletronico extends SoftplanCaixaEletronico {
	
	@Override
	public void transferir(String contaOrigem, String contaDestino, double valor) {
		System.out.println("[TesteCaixaEletronico] Implementação teste do caixa Softplan");
	}

}
