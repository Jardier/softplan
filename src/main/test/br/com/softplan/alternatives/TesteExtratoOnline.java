package br.com.softplan.alternatives;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import br.com.softplan.moduloIII.enums.Banco;
import br.com.softplan.moduloIII.facede.IExtratoOnline;
import br.com.softplan.moduloIII.model.TransferenciaBancaria;

@Alternative
@RequestScoped
public class TesteExtratoOnline implements IExtratoOnline{

	private List<TransferenciaBancaria> transferencias;
	
	@PostConstruct
	public void init() {
		transferencias = new ArrayList<TransferenciaBancaria>();
		transferencias.add(new TransferenciaBancaria(Banco.SOFTPLAN.name(), "ACME", "XPTO", 1000));
		transferencias.add(new TransferenciaBancaria(Banco.SOFTPLAN.name(), "001-4", "002-3", 1500.50));
		transferencias.add(new TransferenciaBancaria(Banco.SOFTPLAN.name(), "005-4", "007-1", 1700.50));
	}

	@Override
	@Produces
	@Named("transferenciasSoftplan")
	@RequestScoped
	public List<TransferenciaBancaria> getTransferencias() {
		return transferencias;
	}

	

}
