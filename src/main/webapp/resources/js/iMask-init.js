window.addEvent('domready', function() {
	new iMask({  
	    onFocus: function(obj) {  
	        obj.setStyles({"background-color":"#ff8"});  
	    },  
	  
	    onBlur: function(obj) {  
	        obj.setStyles({"background-color":"#fff"});  
	    },  
	  
	    onValid: function(event, obj) {  
	        obj.setStyles({"background-color":"#8f8"});  
	    },  
	  
	    onInvalid: function(event, obj) {  
	        if(!event.shift) {  
	            obj.setStyles({"background-color":"#f88"});  
	        }  
	    }  
	});
});